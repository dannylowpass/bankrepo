/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author dannylowpass
 */
public class Bank {
    
    private String bankName;
    
    public Bank(String bankName){
        this.bankName = bankName;
    }
    
    public String getBankName(){
        return bankName; 
    }
    
    public void setBankName(String bankName){
        this.bankName = bankName;
    }
}
