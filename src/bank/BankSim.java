/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author dannylowpass
 */
public class BankSim {
   
    public static void main(String[] args){
       
        Bank bank = new Bank("Royal Bank");
        Employee emp = new Employee("Jose");
        
        System.out.println(emp.getEmpName() + " is an employee of "
                + bank.getBankName());
    }
}
