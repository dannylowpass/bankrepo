/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author dannylowpass
 */
public class Employee {
    
    private String empName;
    
    public Employee(String empName){
        this.empName = empName;
    }
    
    public String getEmpName(){
        return this.empName;
    }
    
    public void setEmpName(String empName){
        this.empName = empName;
    }
}
